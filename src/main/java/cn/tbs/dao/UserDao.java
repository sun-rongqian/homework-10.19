package cn.tbs.dao;

/**
 * @Auther: Sun
 * @Date: 2023/10/19 - 10 - 19 - 10:44
 * @Description: cn.tbs.dao
 * @version: 1.0
 */
public interface UserDao {
    public void getUser();
}
