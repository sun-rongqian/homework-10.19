package cn.tbs.dao.impl;

import cn.tbs.dao.UserDao;

/**
 * @Auther: Sun
 * @Date: 2023/10/19 - 10 - 19 - 15:28
 * @Description: cn.tbs.dao.impl
 * @version: 1.0
 */
public class UserDaoMySqlImpl implements UserDao {
    @Override
    public void getUser() {
        System.out.println("从MySQL数据库获取数据。");
    }
}
