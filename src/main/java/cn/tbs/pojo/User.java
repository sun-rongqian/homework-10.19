package cn.tbs.pojo;

import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @Auther: Sun
 * @Date: 2023/10/19 - 10 - 19 - 16:09
 * @Description: cn.tbs.pojo
 * @version: 1.0
 */
@Data
public class User {
    private String username;
    private int age;

    //数组
    private String[] array;
    //集合
    private List[] list;
    private Set[] set;
    private Map[] map;
    private Properties prop;
}
