package cn.tbs.service;

/**
 * @Auther: Sun
 * @Date: 2023/10/19 - 10 - 19 - 10:48
 * @Description: cn.tbs.dao.cn.tbs.service
 * @version: 1.0
 */
public interface UserService {
    void getUser();
}
