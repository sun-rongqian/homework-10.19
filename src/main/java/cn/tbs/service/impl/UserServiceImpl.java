package cn.tbs.service.impl;

import cn.tbs.dao.UserDao;
import cn.tbs.service.UserService;

/**
 * @Auther: Sun
 * @Date: 2023/10/19 - 10 - 19 - 15:30
 * @Description: cn.tbs.service.impl
 * @version: 1.0
 */
public class UserServiceImpl implements UserService {
    private UserDao userDao;

    @Override
    public void getUser() {
        /*UserDao userDao = new UserDaoMySqlImpl();
        userDao.getUser();*/
        /*ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao userDao = context.getBean("userDao",UserDao.class);
        userDao.getUser();*/
        userDao.getUser();
    }

    public UserServiceImpl() {
    }

    public UserServiceImpl(UserDao userDao){
        this.userDao = userDao;
    }
}
