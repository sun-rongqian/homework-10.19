package cn.tbs.service.impl;

import cn.tbs.dao.UserDao;
import cn.tbs.service.UserService;

/**
 * @Auther: Sun
 * @Date: 2023/10/19 - 10 - 19 - 15:30
 * @Description: cn.tbs.service.impl
 * @version: 1.0
 */
public class UserServiceImpl3 implements UserService {
    private UserDao userDao;

    @Override
    public void getUser() {
        userDao.getUser();
    }

    public void setUserDao(UserDao userDao){
        this.userDao = userDao;
    }
}
