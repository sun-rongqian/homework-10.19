package cn.tbs;

import cn.tbs.dao.UserDao;
import cn.tbs.pojo.User;
import cn.tbs.service.UserService;
import cn.tbs.service.impl.UserServiceImpl;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Auther: Sun
 * @Date: 2023/10/19 - 10 - 19 - 15:57
 * @Description: cn.tbs
 * @version: 1.0
 */
public class MyTest {
    @Test
    public void t1(){
        UserService userService = new UserServiceImpl();
        userService.getUser();
    }

    @Test
    public void t2(){
        //获取IOC容器
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao userDao = context.getBean("userDao",UserDao.class);
        userDao.getUser();
    }

    @Test
    public void t3(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao userDao = context.getBean("userDao",UserDao.class);
        UserDao userDao2 = context.getBean("userDao",UserDao.class);
        System.out.println(userDao);
        System.out.println(userDao2);
        context.close();
    }

    //构造注入
    @Test
    public void t4(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService userService = context.getBean("userService",UserService.class);
        userService.getUser();
        /*UserService userService2 = context.getBean("userService",UserService.class);
        userService2.getUser();*/
    }

    //set注入
    @Test
    public void t5(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService userService = context.getBean("userService2",UserService.class);
        userService.getUser();
    }

    //p命名空间
    @Test
    public void t6(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService userService = context.getBean("userService3",UserService.class);
        userService.getUser();
    }

    //注入简单类型
    @Test
    public void t7(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        User user = context.getBean("user",User.class);
        System.out.println(user);
    }

    //注入集合
    @Test
    public void t8(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        User user = context.getBean("user",User.class);
        System.out.println(user);
    }

    @Test
    public void t9(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext-service.xml","applicationContext-dao.xml");
        UserService userService = context.getBean("userService", UserService.class);
        userService.getUser();
        UserService userService2 = context.getBean("userService2", UserService.class);
        userService2.getUser();
    }

    @Test
    public void t10(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext2.xml");
        UserService userService = context.getBean("userService", UserService.class);
        userService.getUser();
        UserService userService2 = context.getBean("userService2", UserService.class);
        userService2.getUser();
    }
}
